import numpy as np
import cv2

slika = cv2.imread('slike/baboon.bmp',1)

crvena_slika=slika.copy()
crvena_slika[:,:,0]=0
crvena_slika[:,:,1]=0

zelena_slika=slika.copy()
zelena_slika[:,:,0]=0
zelena_slika[:,:,2]=0

plava_slika=slika.copy()
plava_slika[:,:,1]=0
plava_slika[:,:,2]=0

cv2.imwrite("slike/crvena.png",crvena_slika)
cv2.imwrite("slike/plava.png",plava_slika)
cv2.imwrite("slike/zelena.png",zelena_slika)