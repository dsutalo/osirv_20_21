import numpy
import cv2

slika = cv2.imread('slike/goldhill.bmp',1)
print(slika.shape)

prva=slika[::2,:,:].copy()
druga=slika[:,::2,:].copy()
treca=slika[::2,::2,:].copy()

cv2.imshow("prva",prva)
cv2.imshow("druga",druga)
cv2.imshow("treca",treca)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imwrite("slike/redak.jpg",prva)
cv2.imwrite("slike/stupac.jpg",druga)
cv2.imwrite("slike/oboje.jpg",treca)