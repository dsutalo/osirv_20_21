import numpy as np
import cv2
import matplotlib.pyplot as plt


boats=cv2.imread("../images/boats.bmp",0)
airplane=cv2.imread("../images/airplane.bmp",0)

def show(img):
    cv2.imshow("title", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def gaussian_noise(img, mu, sigma):
    out = img.astype(np.float32)
    noise = np.random.normal(mu, sigma, img.shape)
    out = out + noise
    out[out<0] = 0
    out[out>255] = 255
    return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
    out = img.astype(np.float32)
    limit = ((float(percent)/2.0)/100.0) * 255.0
    noise = np.random.uniform(0,255, img.shape)
    out[noise<limit] = 0
    out[noise>(255-limit)] = 255
    out[out>255] = 255
    out[out<0] = 0
    return out.astype(np.uint8)

corupted_saltpep_boats1=(salt_n_pepper_noise(boats,1))
corupted_saltpep_boats2=(salt_n_pepper_noise(boats,10))
corupted_gauss_boats1=(gaussian_noise(boats,0,5))
corupted_gauss_boats2=(gaussian_noise(boats,0,15))
corupted_gauss_boats3=(gaussian_noise(boats,0,35))

corupted_saltpep_airplane1=(salt_n_pepper_noise(airplane,1))
corupted_saltpep_airplane2=(salt_n_pepper_noise(airplane,10))
corupted_gauss_airplane1=(gaussian_noise(airplane,0,5))
corupted_gauss_airplane2=(gaussian_noise(airplane,0,15))
corupted_gauss_airplane3=(gaussian_noise(airplane,0,35))

#def medianblr(slika,a):
 #   vraca=cv2.medianBlur(slika,a)
  #  return vraca

saltpep1radius3=cv2.medianBlur(corupted_saltpep_boats1,3)
saltpep1radius5=cv2.medianBlur(corupted_saltpep_boats1,5)
saltpep1radius7=cv2.medianBlur(corupted_saltpep_boats1,7)

images=[corupted_saltpep_boats1,corupted_saltpep_boats2,corupted_gauss_boats1,corupted_gauss_boats2,corupted_gauss_boats3,corupted_saltpep_airplane1,corupted_saltpep_airplane2,corupted_gauss_airplane1,corupted_gauss_airplane2,corupted_gauss_airplane3]
medislike3=[]
gausslike5=[]
for i in range(len(images)):#djelovanje s medianom od 3
    medislike3.append(cv2.medianBlur(images[i],3))

for i in range(len(images)):#gauss blur s krenelom 5x5
    gausslike5.append(cv2.GaussianBlur(images[i],(5, 5),5))

for i in range(len(gausslike5)):#prikaz slika s gaussom
    cv2.imshow("title", gausslike5[i])
    cv2.waitKey(0)
    cv2.destroyAllWindows()


for i in range(len(medislike3)):#prikaz slika s medianom
    cv2.imshow("title", medislike3[i])
    cv2.waitKey(0)
    cv2.destroyAllWindows()

for i in range(len(gausslike5)):#u istoj for petlji spremamo i gauss i median jer i tako imaju istu duljinu
    cv2.imwrite("slike/zad2/Gauss_Blur/gaussblur"+str(i)+".bmp",gausslike5[i])
    cv2.imwrite("slike/zad2/median_blur/medianblur"+str(i)+".bmp",medislike3[i])












