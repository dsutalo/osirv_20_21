import numpy as np
import cv2
import matplotlib.pyplot as plt


slika=cv2.imread("../images/airplane.bmp")
def show(img):
    cv2.imshow("title", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
def gaussian_noise(img, mu, sigma):
    out = img.astype(np.float32)
    noise = np.random.normal(mu, sigma, img.shape)
    out = out + noise
    out[out<0] = 0
    out[out>255] = 255
    return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
    out = img.astype(np.float32)
    limit = ((float(percent)/2.0)/100.0) * 255.0
    noise = np.random.uniform(0,255, img.shape)
    out[noise<limit] = 0
    out[noise>(255-limit)] = 255
    out[out>255] = 255
    out[out<0] = 0
    return out.astype(np.uint8)

def uniform_noise(img, low, high):
    out = img.astype(np.float32)
    noise = np.random.uniform(low,high, img.shape)
    out = out + noise
    out[out>255] = 255
    out[out<0] = 0
    return out.astype(np.uint8)

def showhist(img):
    hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
    plt.vlines(np.arange(len(hist)), 0, hist)
    plt.title("Histogram")
    plt.show()



gauss1=(gaussian_noise(slika, 0, 1))
gauss2=(gaussian_noise(slika, 0, 5))
gauss3=(gaussian_noise(slika, 0, 10))
gauss4=(gaussian_noise(slika, 0, 20))
gauss5=(gaussian_noise(slika, 0, 40))
gauss6=(gaussian_noise(slika, 0, 60))

uni1=(uniform_noise(slika, -20, 20))
uni2=(uniform_noise(slika, -40, 40))
uni3=(uniform_noise(slika, -60, 60))

saltpep1=(salt_n_pepper_noise(slika,5))
saltpep2=(salt_n_pepper_noise(slika,10))
saltpep3=(salt_n_pepper_noise(slika,15))
saltpep4=(salt_n_pepper_noise(slika,20))



#show(gaussian_noise(slika, 0, 1))
#show(gaussian_noise(slika, 0, 5))
#show(gaussian_noise(slika, 0, 10))
#show(gaussian_noise(slika, 0, 20))
#show(gaussian_noise(slika, 0, 40))
#show(gaussian_noise(slika, 0, 60))

#show(uniform_noise(slika, -20, 20))
#show(uniform_noise(slika, -40, 40))
#show(uniform_noise(slika, -60, 60))

#show(salt_n_pepper_noise(slika,5))
#show(salt_n_pepper_noise(slika,10))
#show(salt_n_pepper_noise(slika,15))
#show(salt_n_pepper_noise(slika,20))

showhist(gauss1)
showhist(gauss2)
showhist(gauss3)
showhist(gauss4)
showhist(gauss5)
showhist(gauss6)
showhist(uni1)
showhist(uni2)
showhist(uni3)
showhist(saltpep1)
showhist(saltpep2)
showhist(saltpep3)

"""cv2.imwrite("slike/gauss1.bmp",gauss1)
cv2.imwrite("slike/gauss5.bmp",gauss2)
cv2.imwrite("slike/gauss10.bmp",gauss3)
cv2.imwrite("slike/gauss20.bmp",gauss4)
cv2.imwrite("slike/gauss40.bmp",gauss5)
cv2.imwrite("slike/gauss60.bmp",gauss6)

cv2.imwrite("slike/uni20.bmp",uni1)
cv2.imwrite("slike/uni40.bmp",uni2)
cv2.imwrite("slike/uni60.bmp",uni3)

cv2.imwrite("slike/saltpep5.bmp",saltpep1)
cv2.imwrite("slike/saltpep10.bmp",saltpep2)
cv2.imwrite("slike/saltpep15.bmp",saltpep3)
cv2.imwrite("slike/saltpep20.bmp",saltpep4)"""
















