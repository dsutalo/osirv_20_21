import numpy as np
import cv2
import matplotlib.pyplot as plt

boats=cv2.imread("C:/Users/filip_000/Desktop/labosi/obrada slike/osirv_labs/osirv_labs/images/boats.bmp",0)
airplane=cv2.imread("C:/Users/filip_000/Desktop/labosi/obrada slike/osirv_labs/osirv_labs/images/airplane.bmp",0)
baboon=cv2.imread("C:/Users/filip_000/Desktop/labosi/obrada slike/osirv_labs/osirv_labs/images/baboon.bmp",0)

#threshold za boats
ret,threshboats1 = cv2.threshold(boats,127,255,cv2.THRESH_BINARY)
ret,threshboats2 = cv2.threshold(boats,127,255,cv2.THRESH_BINARY_INV)
ret,threshboats3 = cv2.threshold(boats,127,255,cv2.THRESH_TRUNC)
ret,threshboats4 = cv2.threshold(boats,127,255,cv2.THRESH_TOZERO)
ret,threshboats5 = cv2.threshold(boats,127,255,cv2.THRESH_TOZERO_INV)
ret,threshboats6 = cv2.threshold(boats,127,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)#otsu threshold
th1 = cv2.adaptiveThreshold(boats,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
                            cv2.THRESH_BINARY,11,2)#threshold vrijednost je srednja vrijednost susjednog podrucja
th2 = cv2.adaptiveThreshold(boats,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                            cv2.THRESH_BINARY,11,2)#gaussov threshold

boats_threshold = [boats,threshboats1,threshboats2,threshboats3,threshboats4,threshboats5,threshboats6,th1,th2]

"""for i in boats_threshold:#prikaz slika #prikaz slika
    cv2.imshow("title", i)
    cv2.waitKey(0)
    cv2.destroyAllWindows()"""
for i in range(len(boats_threshold)):#spremanje boatova
    cv2.imwrite("slike/zad3/boats_thresh"+str(i)+".bmp",boats_threshold[i])

#threshold za airplane
ret,threshairplane1 = cv2.threshold(airplane,127,255,cv2.THRESH_BINARY)
ret,threshairplane2 = cv2.threshold(airplane,127,255,cv2.THRESH_BINARY_INV)
ret,threshairplane3 = cv2.threshold(airplane,127,255,cv2.THRESH_TRUNC)
ret,threshairplane4 = cv2.threshold(airplane,127,255,cv2.THRESH_TOZERO)
ret,threshairplane5 = cv2.threshold(airplane,127,255,cv2.THRESH_TOZERO_INV)
ret,threshairplane6 = cv2.threshold(airplane,127,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)#otsu threshold
th3 = cv2.adaptiveThreshold(airplane,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
                            cv2.THRESH_BINARY,11,2)#threshold vrijednost je srednja vrijednost susjednog podrucja
th4 = cv2.adaptiveThreshold(airplane,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                            cv2.THRESH_BINARY,11,2)#gaussov threshold


airplane_threshold=[airplane,threshairplane1,threshairplane2,threshairplane3,threshairplane4,threshairplane5,threshairplane6,th3,th4]

"""for i in airplane_threshold:#prikaz slika
    cv2.imshow("title",i)
    cv2.waitKey(0)
    cv2.destroyAllWindows()"""
for i in range(len(airplane_threshold)):
    cv2.imwrite("slike/zad3/airplane_thresh"+str(i)+".bmp",airplane_threshold[i])#spremanje airplanea"""

#threshold za baboona
ret,threshbaboon1 = cv2.threshold(baboon,127,255,cv2.THRESH_BINARY)
ret,threshbaboon2 = cv2.threshold(baboon,127,255,cv2.THRESH_BINARY_INV)
ret,threshbaboon3 = cv2.threshold(baboon,127,255,cv2.THRESH_TRUNC)
ret,threshbaboon4 = cv2.threshold(baboon,127,255,cv2.THRESH_TOZERO)
ret,threshbaboon5 = cv2.threshold(baboon,127,255,cv2.THRESH_TOZERO_INV)
ret,threshbaboon6 = cv2.threshold(baboon,127,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)#otsu threshold
th5 = cv2.adaptiveThreshold(baboon,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
                            cv2.THRESH_BINARY,11,2)#threshold vrijednost je srednja vrijednost susjednog podrucja
th6 = cv2.adaptiveThreshold(baboon,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                            cv2.THRESH_BINARY,11,2)#gaussov threshold


baboon_threshold=[baboon,threshbaboon1,threshbaboon2,threshbaboon3,threshbaboon4,threshbaboon5,threshbaboon6,th5,th6]

"""for i in baboon_threshold:#prikaz slika
    cv2.imshow("title",i)
    cv2.waitKey(0)
    cv2.destroyAllWindows()"""

for i in range(len(baboon_threshold)):
    cv2.imwrite("slike/zad3/baboon_thresh"+str(i)+".bmp",baboon_threshold[i])






