# Numpy osnove, konvolucija, invert, thresholding, quantisation, noise, geometric transformations

#### Numpy dodatna dokumentacija

[Numpy quickstart](https://docs.scipy.org/doc/numpy/user/quickstart.html) - na
ovom linku nalazi se odličan Numpy tutorial koji će vam ukratko objasniti
osnovne stvari koje smo možda propustili objasniti.

[Numpy User Guide](http://docs.scipy.org/doc/numpy/user/index.html#user)

#### Upute za rješavanje 

- Napravite novi direktorij ` report ` u direktoriju ove vježbe.
- U njemu napravite file ` README.md ` koji će sadržavati izvještaj vaših rješenja
- Riješene programe  i rezultantne slike za svaki od zadataka spremate u direktorij ` report/problemN `, 
gdje je ` N ` broj riješavanog zadatka.
- U izvještaj (`report/README.md`) ćete dodati snippete koda koji riješavaju traženi problem,
 kao i dobivene slike.
- Pogledajte u sourceu ovog 
dokumenta kako možete formatirati kod unutar README.md filea.
- Detlji o Markdown sintaksi na [linku](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Numpy osnove

U slijedećim primjerima pretpostavimo da je numpy uključen pomoću slijedeće
naredbe:

```python
#Nacin 1
from numpy  import *
``` 
<!--`-->

Na ovaj način uključuju se se naredbe iz modula numpy u namespace, te im se
može pristupati bez pisanja ` np. ` predmetka. Do sada nismo Numpy importali na
ovaj način, već smo koristili: 

```python
#Nacin 2
import numpy as np
```
<!--`-->

Tako za pristup funkciji ` zeros ` u ovim primjerima će biti napisano samo `
zeros `, dok ako se uključi na način 2, pristupat ćemo joj sa ` np.zeros `.
Isto tako mijenjanje tipa polja bi pri prvom načinu izvršavali sa `
polje.astype(uint8) `, dok u drugom načinu bi bio ` polje.astype(np.uint8) `.


#### Kreiranje novih polja

Postoji više načina kako kreirati numpy polja. Na primjer, moguće je kreirati
polje iz obične Python liste ili tupla koristeći funkciju array. Tip podatka
polja ovisi o tipovima u listi.

```
>>> a = array( [2,3,4] )
>>> a
array([2, 3, 4])
>>> a.dtype
dtype('int32')
>>> b = array([1.2, 3.5, 5.1])
>>> b.dtype
dtype('float64')
```
<!--` -->

Česta je greška pozivanje fukcije s više numeričkih argumenata, umjesto 

    >>> a = array(1,2,3,4)    # WRONG

    >>> a = array([1,2,3,4])  # RIGHT

Često se događa da je pri stvaranju polja poznata potrebna veličina tog polja,
ali nisu poznate vrijednosti koje će to polje sadržavati. Numpy omogućuje
nekoliko funkcija koje stvaraju polja popunjena nekim određenim početnim
vrijednostima.
Ove funkcije smanjuju potrebu za promjenom veličine polja, što je vrlo skupa
operacija. 

Funkcija ` zeros ` stvara polje puno nula, funkcija ` ones ` stvara polje puno
jedinica, dok fukcija ` empty ` stvara polje čije su početne vrijenosti
neodređene, tj. ovise o trenutnom stanju memorije pri zauzimanju polja.
Defaultni tip podatka u takvim poljima je ` float64 `. 

U slijedećim primjerima prvi parametar koji predajemo je tuple koji sadži
podatke o dimenzijama polja.

    >>> zeros( (3,4) )
    array([[0.,  0.,  0.,  0.],
           [0.,  0.,  0.,  0.],
           [0.,  0.,  0.,  0.]])
    >>> ones( (2,3,4), dtype=int16 )                # dtype can also be specified
    array([[[ 1, 1, 1, 1],
            [ 1, 1, 1, 1],
            [ 1, 1, 1, 1]],
           [[ 1, 1, 1, 1],
            [ 1, 1, 1, 1],
            [ 1, 1, 1, 1]]], dtype=int16)
    >>> empty( (2,3) )
    array([[  3.73603959e-262,   6.02658058e-154,   6.55490914e-260],
           [  5.30498948e-313,   3.14673309e-307,   1.00000000e+000]])

Isto tako, moguće je umjesto predavanja dimenzija polja kreirati polje
identično po veličini i tipu podatka nekom drugom polju, i to samo jednom
naredbom. 

    >>> a = np.zeros( (3,4), dtype = np.float32 )
    >>> a
    array([[ 0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.]], dtype=float32)
    >>> b = np.zeros_like(a)
    >>> b
    array([[ 0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.]], dtype=float32)
    >>> c = np.ones_like(a)
    >>> c
    array([[ 1.,  1.,  1.,  1.],
           [ 1.,  1.,  1.,  1.],
           [ 1.,  1.,  1.,  1.]], dtype=float32)
    >>> d = np.empty_like(a)
    >>> d
    array([[  5.70919145e-11,   7.42688186e-44,   3.86304199e-16,
              0.00000000e+00],
           [  0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
              0.00000000e+00],
           [  6.51153190e-16,   0.00000000e+00,   1.40129846e-45,
              0.00000000e+00]], dtype=float32)

Kako bi kreirali sekvencu brojeva može se koristiti slijedeća naredba:

    >>> arange( 10, 30, 5 )
    array([10, 15, 20, 25])
    >>> arange( 0, 2, 0.3 )                 # it accepts float arguments
    array([ 0. ,  0.3,  0.6,  0.9,  1.2,  1.5,  1.8])

Kada se koristi ova funkcija za realne brojeve, najčešće nije moguće
pretpostaviti koliko će imati elemenata, jer realni brojevi imaju konačnu
točnost. Zato je bolje koristiti funkciju ` linspace `, koja prima kao
argument broj elemenata koji želimo, umjesto parametra koraka.

    >>> linspace( 0, 2, 9 )                 # 9 numbers from 0 to 2
    array([ 0.  ,  0.25,  0.5 ,  0.75,  1.  ,  1.25,  1.5 ,  1.75,  2.  ])
    >>> x = linspace( 0, 2*pi, 100 )        # useful to evaluate function at lots of points
    >>> f = sin(x)


####Kopiranje i viewovi

    >>> a = np.eye(10)
    >>> a
    array([[ 1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.]])
    >>> b = a[3:7,3:7]
    >>> b
    array([[ 1.,  0.,  0.,  0.],
           [ 0.,  1.,  0.,  0.],
           [ 0.,  0.,  1.,  0.],
           [ 0.,  0.,  0.,  1.]])
    >>> b[:] = 9
    >>> b
    array([[ 9.,  9.,  9.,  9.],
           [ 9.,  9.,  9.,  9.],
           [ 9.,  9.,  9.,  9.],
           [ 9.,  9.,  9.,  9.]])
    >>> a
    array([[ 1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  9.,  9.,  9.,  9.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  9.,  9.,  9.,  9.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  9.,  9.,  9.,  9.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  9.,  9.,  9.,  9.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.],
           [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.]])
    >>> b = 10
    >>> b
    10

#### Problem 1 - Numpy basics

- Dodatna dokumentacija za numpy nalazi se na vrhu dokumenta
- Učitajte iz foldera ` images ` ( rootu repoa) tri različite slike, te ih spojite u jednu
sliku jednu do druge. Prikažite i spremite tu sliku. Koristiti funkcije `hstack` ili
`vstack`. Dozvoljeno je odsjeći dio slike kako bi svi dijelovi bili istih
dimenzija. 
- Učitajte sliku i dodajte crni okvir oko slike, ali na način da ne
prepisujete dijelove slike, već da dodate okvir na rub slike. Kod za dodavanje
okvira stavite u funkciju i omogućite dodavanje proizvoljne širine okvira
(širina se predaje funkciji kao parametar). 


## Konvolucija

[Konvolucija wikipedia](https://en.wikipedia.org/wiki/Kernel_%28image_processing%29)

U ovom dijelu vježbe ćemo implementirati funkciju konvolucije. Konvoluciju
možemo smatrati kao zamjenu svakog piksela sa težinskom sumom njegovih susjeda.
Kernel je zapravo druga slika, najčešće puno manja od slike koju konvoluiramo,
te kernel zapravo sadrži težine. Suma svih težina u kernelu trebala bi biti 1
(jedan). 

Slijedi algoritam konvolucije:

    for each image row in output image:
       for each pixel in image row:

          set running total to zero

          for each kernel row in kernel:
             for each element in kernel row:

                multiply element value by corresponding* pixel value
                add result to running total

          set output image pixel to value of running total


    *corresponding input image pixels are found relative to the kernel's origin. 


Potrebno je pripaziti da se ne pristupa memoriji izvan granica slike. 
Konvolucija inače najčešće zahtjeva i pristup pikselima izvan slike. Budući da
to nije moguće, taj problem se može riješiti na više načina: 

1. **Extend** - Tako što će se rubni pikseli slike duplicirati koliko god je potrebno kako
bi kernel počeo od početka slike
2. **Wrap** - Vrijednosti piksela na lokacijama izvan slike uzimaju se sa
suprotnog ruba slike.
3. **Crop** - Bilo koji piksel izlazne slike koji bi zahtjevao konvoluciju sa
pikselom izvan slike se odbacuje. Rezultat ove metode je slika koja je manja za
dimenzije kernela -1 piksel.

U kodu dolje implementirana je konvolucija koja koristi metodu 3. 


```python
def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 
```
<!--`-->


#### Problem 2 - Konvolucija

- Izvrtite konvoluciju na slici po izboru za primjere kernela sa [linka](https://en.wikipedia.org/wiki/Kernel_%28image_processing%29) .
- Kod i rezultantne slike dodajte u report.


## Problem 3 - Invert

- Create a program which will load all images from `images` directory in grayscale 
- perform the invert operation and 
- save the images to `problem3/imagename_invert.png`.

## Problem 4 - Threshold

- Create a program which will load all images from `images` directory
- perform the threshold operation with the parameter values of `63`, `127`, `191`
  - threshold operation should set all pixels with values `<= threshold` to 0 and leave
    the values `>= threshold` as they are.
- save the images to `problem4/imagename_parametervalue_thresh.png`

## Problem 5 - Quantisation 
Usually, the grey values in an image are represented by $`2^8 = 256`$
different grey values. In this problem, we want to quantise the image
`BoatsColor.bmp` such that it contains afterwards only $`2^q`$ different grey
values, with $`q < 8`$.

In order to reduce the number of grey values we subdivide the co-domain into
uniform intervals of size $` d = 2^{8-q} `$. All values that lie in an
interval are then mapped to the mean value of the interval: 

![quantize]( http://latex.codecogs.com/gif.latex?u_%7Bi%2C%20j%7D%20%3A%3D%20%5Cleft%20%28%20%5Cleft%20%5Clfloor%20%5Cfrac%7B%20u_%7Bi%2Cj%7D%20%7D%7Bd%7D%20%5Cright%20%5Crfloor%20&plus;%20%5Cfrac%7B1%7D%7B2%7D%20%5Cright%20%29%20*%20d)

Where $` \lfloor . \rfloor `$ denotes the floor function, i.e. 
$` \lfloor x \rfloor `$ gives the largest integer number that does not exceed
$` x `$.

#### Your task: 

- Perform the quantisation on the image 
` BoatsColor.bmp` 
for all values of $` q `$ in the interval  $` [ 1, 8 ] `$ . 
- Save the images as
`.bmp` files with the filename format ` boats_q.bmp `,  where ` q ` is the
numeric value of $` q `$ used for that image.  
- Comment the visual quality of quantised images compared to original image, for
all  values of  $` q `$.

[]()


##### Hints:

If ` img ` is numpy array containing your image: 

- Load the image as grayscale ( ` cv2.CV_LOAD_IMAGE_GRAYSCALE ` )
- Convert the image to _floats_ before processing ( ` img =
img.astype(np.float32) ` ) 
- Values greater than 255 set to 255, and values lower than 0 set to 0
- `np.max(img)` returns the
maximum value in the array, `np.min(img)` returns minimum value and
`np.unique(img)` returns a list of unique values in the array.
- `np.floor(img)` returns an array with _floored_ values.
- After processing, return the image back to ` np.uint8 ` dtype.


## Problem 6 - Noise

Integrate uniformly distributed noise $` n_{i,j} `$ with zero mean and
maximal deviation of $` 0.5 `$ into the quantisation process:

$` u_{i, j} :=  \left ( \left \lfloor \frac{ u_{i,j} }{d} + n_{i,j} \right \rfloor + \frac{1}{2} \right ) * d `$


#### Your task:

- Perform the quantisation with the added noise, according to the formula
above. Use the same image and quantisation values as in Problem 3.
- Save the images as
`.bmp` files with the filename format ` boats_qn.bmp `,  where ` q ` is the
numeric value of $` q `$ used for that image.  
- Comment the visual quality of quantised images with noise compared to
quantised images in Problem 3, for all the values of $` q `$.

##### Hint:

- For generation of uniform noise use `np.random.uniform(0,1, shape )` numpy
function (`shape` is the shape of resulting array). 
See [Numpy
documentation](http://docs.scipy.org/doc/numpy/reference/routines.random.html).



## Problem 7 - Geometric transformations of Images


OpenCV provides two transformation functions, cv2.warpAffine and
cv2.warpPerspective, with which you can have all kinds of transformations.
cv2.warpAffine takes a 2x3 transformation matrix while cv2.warpPerspective
takes a 3x3 transformation matrix as input.

### Scaling

Scaling is just resizing of the image. OpenCV comes with a function
`cv2.resize()` for this purpose. The size of the image can be specified manually,
or you can specify the scaling factor. Different interpolation methods are
used. Preferable interpolation methods are `cv2.INTER_AREA` for shrinking and
`cv2.INTER_CUBIC` (slow) or  `cv2.INTER_LINEAR` for zooming. By default,
interpolation method used is `cv2.INTER_LINEAR` for all resizing purposes. You
can resize an input image either of following methods:

```python
import cv2
import numpy as np

def show(img):
  cv2.imshow("title", img)
  cv2.moveWindow("title", 800, 200)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

img = cv2.imread('../slike/baboon.bmp')
res = cv2.resize(img,None,fx=0.75, fy=0.25, interpolation = cv2.INTER_CUBIC)
show(res)

#OR

height, width = img.shape[:2]
res = cv2.resize(img,(2*width, 2*height), interpolation = cv2.INTER_CUBIC)
show(res)
```
<!--`-->

### Translation

_For translation and rotation of images we will use affine transformations. On
http://homepages.inf.ed.ac.uk/rbf/HIPR2/affine.htm is excellent explanation of
affine transforms._

Translation is the shifting of object’s location. If you know the shift in
$`(x,y)`$ direction, let it be $`(t_x,t_y)`$, you can create the transformation matrix
$`\textbf{M}`$ as follows:

$`M = \begin{bmatrix} 1 & 0 & t_x \\
0 & 1 & t_y \end{bmatrix}`$

You can then make it into a Numpy array of type `np.float32` and pass it into
` cv2.warpAffine() ` function. See below example for a shift of (100,50):

```
import cv2
import numpy as np

def show(img):
  cv2.imshow("title", img)
  cv2.moveWindow("title", 800, 200)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

img = cv2.imread('../slike/baboon.bmp', cv2.CV_LOAD_IMAGE_GRAYSCALE)
rows,cols = img.shape

M = np.float32([[1,0,100],[0,1,50]])
dst = cv2.warpAffine(img,M,(cols,rows))

cv2.imshow('img',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
```
<!--`-->

**Warning**: 
Third argument of the `cv2.warpAffine()` function is the size of the output
image, which should be in the form of (width, height). Remember width = number
of columns, and height = number of rows.


### Rotation 

Rotation of an image for an angle $`\theta`$ is achieved by the transformation
matrix of the form

$`M = \begin{bmatrix} cos\theta & -sin\theta & t_x \\
sin\theta & cos\theta & t_y \end{bmatrix}`$

<!--`-->


```
#### Rotation
import cv2
import numpy as np
from math import sin, cos, pi

def show(img):
  cv2.imshow("title", img)
  cv2.moveWindow("title", 800, 200)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

img = cv2.imread('../slike/baboon.bmp', cv2.CV_LOAD_IMAGE_GRAYSCALE)
rows,cols = img.shape

#theta should be in radians, while tx and ty should be in pixels
deg = 45
theta = (deg * pi) / 180
tx = 100
ty = 100

M = np.float64([[ cos(theta), -sin(theta), tx],\
                [ sin(theta),  cos(theta), ty]])
print M
dst = cv2.warpAffine(img,M,(cols,rows))

show(dst)
```
<!--`-->

As you can see in the above example, this operation rotates an image around the
$`(0,0)`$ coordinate.

But OpenCV provides scaled rotation with adjustable center of rotation so that
you can rotate at any location you prefer. Modified transformation matrix is
given by

$`\begin{bmatrix} \alpha & \beta & (1- \alpha ) \cdot center.x - \beta \cdot center.y \\
-\beta & \alpha & \beta \cdot center.x + (1- \alpha ) \cdot center.y \end{bmatrix}`$

where:

$`\begin{array}{l} \alpha = scale \cdot \cos \theta, \\
\beta = scale \cdot \sin \theta \end{array}`$

To find this transformation matrix, OpenCV provides a function,
`cv2.getRotationMatrix2D`. Check below example which rotates the image by 90
degree with respect to center without any scaling.

```
import cv2
import numpy as np
from math import sin, cos, pi

def show(img):
  cv2.imshow("title", img)
  cv2.moveWindow("title", 800, 200)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

img = cv2.imread('../slike/baboon.bmp', cv2.CV_LOAD_IMAGE_GRAYSCALE)
rows,cols = img.shape

M = cv2.getRotationMatrix2D((cols/2,rows/2),90,1) 
dst = cv2.warpAffine(img,M,(cols,rows))

show(dst)
```



#### Your task:

- Create a following 
[image](https://www.dropbox.com/s/5a7wxbk9zy782v3/4ex1.png?dl=1):
by iteratively rotating `baboon` image for 30 degrees between iterations, and
stacking results together. Before applying rotation to the image, scale the
image to quarter of its width and height.

