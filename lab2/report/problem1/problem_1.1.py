import numpy as np 
import cv2

slika1=cv2.imread("../../slike/baboon.bmp",1)
slika2=cv2.imread("../../slike/airplane.bmp",1)
slika3=cv2.imread("../../slike/barbara.bmp",1)

novaslika1=slika2[0:480,:,:]
novaslika2=slika3[0:480,:,:]

novaslika3=slika2[:,0:500,:]
novaslika4=slika3[:,0:500,:]

gotovaslika1=np.hstack((slika1,novaslika1,novaslika2))
gotovaslika2=np.vstack((slika1,novaslika3,novaslika4))

cv2.imwrite("slike/horizontalno.jpg",gotovaslika1)
cv2.imwrite("slike/vertikalno.jpg",gotovaslika2)
cv2.imshow("hori", gotovaslika1)
cv2.imshow("vert",gotovaslika2)
cv2.waitKey(0)
cv2.destroyAllWindows()