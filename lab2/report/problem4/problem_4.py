import matplotlib.image as mpimg
import numpy as np
import cv2


slika1=cv2.imread("../../slike/airplane.bmp",0)
slika2=cv2.imread("../../slike/baboon.bmp",0)
slika3=cv2.imread("../../slike/barbara.bmp",0)
slika4=cv2.imread("../../slike/boats.bmp",0)
slika5=cv2.imread("../../slike/pepper.bmp",0)
slika6=cv2.imread("../../slike/goldhill.bmp",0)
slika7=cv2.imread("../../slike/lenna.bmp",0)

#Threshold slike1
_,slika1thresh63=cv2.threshold(slika1,63,255,cv2.THRESH_BINARY)
_,slika1thresh127=cv2.threshold(slika1,127,255,cv2.THRESH_BINARY)
_,slika1thresh191=cv2.threshold(slika1,191,255,cv2.THRESH_BINARY)
#slike2
_,slika2thresh63=cv2.threshold(slika2,63,255,cv2.THRESH_BINARY)
_,slika2thresh127=cv2.threshold(slika2,127,255,cv2.THRESH_BINARY)
_,slika2thresh191=cv2.threshold(slika2,191,255,cv2.THRESH_BINARY)
#slike3
_,slika3thresh63=cv2.threshold(slika3,63,255,cv2.THRESH_BINARY)
_,slika3thresh127=cv2.threshold(slika3,127,255,cv2.THRESH_BINARY)
_,slika3thresh191=cv2.threshold(slika3,191,255,cv2.THRESH_BINARY)
#slike4
_,slika4thresh63=cv2.threshold(slika4,63,255,cv2.THRESH_BINARY)
_,slika4thresh127=cv2.threshold(slika4,127,255,cv2.THRESH_BINARY)
_,slika4thresh191=cv2.threshold(slika4,191,255,cv2.THRESH_BINARY)
#slike5
_,slika5thresh63=cv2.threshold(slika5,63,255,cv2.THRESH_BINARY)
_,slika5thresh127=cv2.threshold(slika5,127,255,cv2.THRESH_BINARY)
_,slika5thresh191=cv2.threshold(slika5,191,255,cv2.THRESH_BINARY)
#slike6
_,slika6thresh63=cv2.threshold(slika6,63,255,cv2.THRESH_BINARY)
_,slika6thresh127=cv2.threshold(slika6,127,255,cv2.THRESH_BINARY)
_,slika6thresh191=cv2.threshold(slika6,191,255,cv2.THRESH_BINARY)
#slike7
_,slika7thresh63=cv2.threshold(slika7,63,255,cv2.THRESH_BINARY)
_,slika7thresh127=cv2.threshold(slika7,127,255,cv2.THRESH_BINARY)
_,slika7thresh191=cv2.threshold(slika7,191,255,cv2.THRESH_BINARY)

thresh63=[slika1thresh63,slika2thresh63,slika3thresh63,slika4thresh63,slika5thresh63,slika6thresh63,slika7thresh63]
thresh127=[slika1thresh127,slika2thresh127,slika3thresh127,slika4thresh127,slika5thresh127,slika6thresh127,slika7thresh127]
thresh191=[slika1thresh191,slika2thresh191,slika3thresh191,slika4thresh191,slika5thresh191,slika6thresh191,slika7thresh191]

lista_imena63=["slike/slika1_63_thresh.png","slike/slika2_63_thresh.png","slike/slika3_63_thresh.png","slike/slika4_63_thresh.png","slike/slika5_63_thresh.png","slike/slika6_63_thresh.png","slike/slika7_63_thresh.png"]
lista_imena127=["slike/slika1_127_thresh.png","slike/slika2_127_thresh.png","slike/slika3_127_thresh.png","slike/slika4_127_thresh.png","slike/slika5_127_thresh.png","slike/slika6_127_thresh.png","slike/slika7_127_thresh.png"]
lista_imena191=["slike/slika1_191_thresh.png","slike/slika2_191_thresh.png","slike/slika3_191_thresh.png","slike/slika4_191_thresh.png","slike/slika5_191_thresh.png","slike/slika6_191_thresh.png","slike/slika7_191_thresh.png"]

cv2.imshow("slika1_63",slika1thresh63)#prikaz slike 1 s 3 parametra thresholda
cv2.imshow("slika1_127",slika1thresh127)
cv2.imshow("slika1_191",slika1thresh191)

for i in range(len(thresh63)):
    cv2.imwrite(lista_imena63[i],thresh63[i])#spremanje iz liste thresh63 kao imena navedena u lista_imena63
    cv2.imwrite(lista_imena127[i],thresh127[i])
    cv2.imwrite(lista_imena191[i],thresh191[i])
    
cv2.waitKey(0)
cv2.destroyAllWindows()





