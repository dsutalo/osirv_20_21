import cv2
import numpy as np
from math import sin, cos, pi

def show(img):
  cv2.imshow("baboon", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

slika=cv2.imread('../../slike/baboon.bmp',0)
#skaliranje na cetvrtinu visine i sirine
skal_slika=cv2.resize(slika,None,fx=0.25,fy=0.25,interpolation = cv2.INTER_CUBIC)
#print(slika.shape)
#show(skal_slika)
#print(skal_slika.shape)
redovi,stupci=skal_slika.shape
slike=[]
#360/30=12 i dvanaest puta rotiram sliku za 30 stupanjeva i sve to spajam u jednu sliku
for i in range(1,13):
    M = cv2.getRotationMatrix2D((redovi/2,stupci/2),30*i,1)
    slike.append(cv2.warpAffine(skal_slika,M,(redovi,stupci)))

#show(slike[11])

slike[0]=cv2.resize(slike[0],(120,125))#resizeamo na velicinu skalirane slike
slike[1]=cv2.resize(slike[1],(120,125))
slike[2]=cv2.resize(slike[2],(120,125))
slike[3]=cv2.resize(slike[3],(120,125))
slike[4]=cv2.resize(slike[4],(120,125))
slike[5]=cv2.resize(slike[5],(120,125))
slike[6]=cv2.resize(slike[6],(120,125))
slike[7]=cv2.resize(slike[7],(120,125))
slike[8]=cv2.resize(slike[8],(120,125))
slike[9]=cv2.resize(slike[9],(120,125))
slike[10]=cv2.resize(slike[10],(120,125))
slike[11]=cv2.resize(slike[11],(120,125))

gotova_slika=np.hstack((slike[0],slike[1],slike[2],slike[3],slike[4],slike[5],slike[6],slike[7],slike[8],slike[9],slike[10],slike[11],slike[12]))
show(gotova_slika)


