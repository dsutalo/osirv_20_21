import numpy as np
import cv2

#gotov kod za konvoluciju :
def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output

kernel1=np.array([[0,0,0],[0,1,0],[0,0,0]])
kernel2=np.array([[1,0,-1],[0,0,0],[-1,0,1]])
kernel3=np.array([[0,1,0],[1,-4,1],[0,1,0]])
kernel4=np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])
kernel5=np.array([[0,-1,0],[-1,5,-1],[0,-1,0]])


slika=cv2.imread("../../slike/baboon.bmp",0)
rez_slika=convolve(slika,kernel1)#identity
rez_slika1=convolve(slika,kernel2)#detekcije rubova
rez_slika2=convolve(slika,kernel3)
rez_slika3=convolve(slika,kernel4)
rez_slika4=convolve(slika,kernel5)#izoštravanje

cv2.imwrite("slike/rez_slika1.jpg",rez_slika)
cv2.imwrite("slike/rez_slika2.jpg",rez_slika1)
cv2.imwrite("slike/rez_slika3.jpg",rez_slika2)
cv2.imwrite("slike/rez_slika4.jpg",rez_slika3)
cv2.imwrite("slike/rez_slika5.jpg",rez_slika4)


cv2.imshow("a",rez_slika)
cv2.imshow("b",rez_slika1)
cv2.imshow("c",rez_slika2)
cv2.imshow("d",rez_slika3)
cv2.imshow("e",rez_slika4)
cv2.waitKey(0)
cv2.destroyAllWindows()

