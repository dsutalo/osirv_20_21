import numpy as np
import cv2

def kvantizacija(q):
    slika=cv2.imread("../../slike/BoatsColor.bmp",0)
    slika=slika.astype(np.float32)#pretvaranje tipa slike
    d=pow(2,8-q)#2^(8-q)
    n=np.random.uniform(0,1,np.shape(slika))#matrica koja predstavlja noise
    print(n)
    visina_slike=np.size(slika,0)#broj redaka
    sirina_slike=np.size(slika,1)#broj stupaca
    for i in range(visina_slike):
        for j in range(sirina_slike):
            slika[i,j]=(np.floor(slika[i,j]+n[i,j])+0.5)*d#dodavanje šuma po formuli
            if (slika[i,j]>255):
                slika[i,j]=255
            elif(slika[i,j]<0):
                slika[i,j]=0
    slika=slika.astype(np.uint8)
    path="C:/OSiRV/osirv_20_21/lab2/report/problem5/slike/boats_"+str(q)+"n.bmp"
    #cv2.imshow("a",slika)
    cv2.imwrite(path,slika)

a=8#slika 8 ce biti s najmanje suma, slika 1 s najvise
while(a>0):
    kvantizacija(a)
    a=a-1
