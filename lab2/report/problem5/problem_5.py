import numpy as np
import cv2


def kvantizacija(q):
    slika=cv2.imread("../../slike/BoatsColor.bmp",0)
    slika=slika.astype(np.float32)#pretvaranje tipa slike
    d=pow(2,8-q)#2^(8-q)
    visina_slike=np.size(slika,0)#broj redaka
    sirina_slike=np.size(slika,1)#broj stupaca
    for i in range(visina_slike):
        for j in range(sirina_slike):
            slika[i,j]=(np.floor(slika[i,j]/d)+0.5)*d
            if (slika[i,j]>255):
                slika[i,j]=255
            elif(slika[i,j]<0):
                slika[i,j]=0

    najveca=np.max(slika)
    najmanja=np.min(slika)
    jedinstvena=np.unique(slika)
    print("najveca vrijednost: ",najveca,"najmanja vrijednost slike: ", najmanja," jedinstvena vrijednost: ",jedinstvena)

    slika=slika.astype(np.uint8)
    #cv2.imshow("a",slika)
    path="C:/OSiRV/osirv_20_21/lab2/report/problem5/slike/boats"+str(q)+".bmp"#spremanje od slike 8 do slike 1(q se smanjuje)
    cv2.imwrite(path,slika)
                
a=8
while(a>0):
    kvantizacija(a)
    a=a-1
    
    
