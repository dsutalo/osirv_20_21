import numpy as np
import cv2

slika1=cv2.imread("../../slike/baboon.bmp",0)
slika2=cv2.imread("../../slike/airplane.bmp",0)
slika3=cv2.imread("../../slike/barbara.bmp",0)
slika4=cv2.imread("../../slike/boats.bmp",0)
slika5=cv2.imread("../../slike/pepper.bmp",0)

def invertiraj(slika):
    slika=(255-slika)
    return slika

slika1=invertiraj(slika1)
slika2=invertiraj(slika2)
slika3=invertiraj(slika3)
slika4=invertiraj(slika4)
slika5=invertiraj(slika5)

cv2.imwrite("slike/inv_slika1.jpg",slika1)
cv2.imwrite("slike/inv_slika2.jpg",slika2)
cv2.imwrite("slike/inv_slika3.jpg",slika3)
cv2.imwrite("slike/inv_slika4.jpg",slika4)
cv2.imwrite("slike/inv_slika5.jpg",slika5)

cv2.imshow("prva",slika1)
cv2.imshow("druga",slika2)
cv2.imshow("treca",slika3)
cv2.imshow("cetvrta",slika4)
cv2.imshow("peta",slika5)
cv2.waitKey(0)
cv2.destroyAllWindows()


